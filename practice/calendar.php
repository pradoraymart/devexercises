<?php
    // Get the current month
    if (!isset($_REQUEST["month"])) {
        $_REQUEST["month"] = date("n");
    } 
    // Get the current year
    if (!isset($_REQUEST["year"])) {
        $_REQUEST["year"] = date("Y");
    }

    $currMonth = $_REQUEST["month"];
    $currYear = $_REQUEST["year"];

    // Store months in an array
    $monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
                        "August", "September", "October", "November", "December");

    $previousYear = $currYear;
    $nextYear = $currYear;
    $previousMonth = $currMonth - 1;
    $nextMonth = $currMonth + 1;

    // Checks the previous month and year
    if ($previousMonth == 0 ) {
        // Set month to December
        $previousMonth = 12;
        // Decrement the current year
        $previousYear = $currYear - 1;
    }

    // Checks the succeeding months and year
    if ($nextMonth == 13 ) {
        // Set month to December
        $nextMonth = 1;
        // Increment the current year
        $nextYear = $currYear + 1;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Calendar</title>
    <style>
        .header{
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        table{
            width: 60%;
            table-layout: fixed;
            margin: 0px 50px 0px 20%;
        }
        th {
            height:20px;
            width:20%;
            vertical-align:middle;
            text-align: center;
            padding:10px;
            color: white;
            background-color: #1E90FF ;
        }
        td {
            height:50px;
            width:20%;
            vertical-align:top;
            text-align: left;
            padding:10px;
        }
        table, th, td {
            border: 5px solid white;
            border-collapse: collapse;
        }
        .navigation {
            text-align: center;
            margin-top:20px;
            margin-bottom:50px;
        }
        .currentDate {
            color:white;
            background-color:#00BFFF;
        }
        .date {
            color:gray;
            background-color:#F0F0F0;
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>Simple Calendar Using PHP</h1>
        <h2><?= $monthNames[$currMonth - 1]. " " .$currYear; ?></h2>
    </div>
    <table>
        <thead>
            <tr>
                <th>Sunday</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $timeStamp = mktime(0, 0, 0, $currMonth, 1, $currYear);
            $endDay = date("t", $timeStamp);
            $thisMonth = getdate ($timeStamp);
            $startDay = $thisMonth['wday'];
            $currentDate = date("d");
            $currentMonth = date("n");
            for ($i = 0; $i < ($endDay + $startDay); $i++) {
                if (($i % 7) == 0 ) {
                    echo "<tr>";
                }
                if ($i < $startDay) {
                    echo "<td></td>";
                } else {
                    $listDate = $i - $startDay + 1;
                    if(($listDate ==  $currentDate) && ($monthNames[$currMonth - 1] == $monthNames[$currentMonth -1])) {
                        echo "<td class='currentDate'>" .$listDate. "</td>";
                    } else {
                        echo "<td class='date'>" .$listDate. "</td>";
                    }
                }
                // Display 7 data per rows
                if (($i % 7) == 6 ) {
                    echo "</tr>";
                }
            }
        ?>
        </tbody>
    </table>
    <div class="navigation">
        <button type="button" name="previous">
            <a href="<?= $_SERVER["PHP_SELF"] . "?month=". $previousMonth . "&year=" . $previousYear; ?>">
            Previous</a>
        </button>
        <button type="button" name="next">
            <a href="<?= $_SERVER["PHP_SELF"] . "?month=". $nextMonth . "&year=" . $nextYear; ?>">
            Next</a>
        </button>
    </div>
</body>
</html>