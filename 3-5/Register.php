<?php
    require_once "db_connection.php";

    $error = "";
    $firstName = "";
    $lastName = "";
    $email = "";

    if ($user->is_loggedin() != "") {
        $user->redirect("Welcome.php");
    }

    if (isset($_POST["submit"])) {
        $firstName = trim($_POST["firstName"]);
        $lastName = trim($_POST["lastName"]);
        $email = trim($_POST["email"]);
        $password = trim($_POST["password"]);
        $allowed = array("jpeg", "png", "jpg");
        $fileName = $_FILES["image"]["name"];
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        if ($firstName == "") {
            $error .= "Please insert your first name !";
        }
        else if ($lastName == "") {
            $error .= "Please insert your last name !";
        }
        else if ($email == "") {
            $error .= "Please insert an email !";
        }
        else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error .= "Please enter a valid email address !";
        }
        else if ($password == "") {
            $error .= "Please insert your password !";
        }
        else if (strlen($password) < 8) {
            $error .= "Password must be atleast 8 characters";
        } 
        else if ($fileName == "") {
            $error .= "Please insert an image";
        } 
        else if (!in_array($ext, $allowed)) { 
            $error .= "Sorry, only JPG, JPEG, PNG & GIF  files are allowed.";
        }
        else {
            try {
                $query = $db_conn->prepare("SELECT email FROM users WHERE email = :email");
                $query->execute(array(":email" => $email));
                $row = $query->fetch(PDO::FETCH_ASSOC);
        
                if ($row["email"] == $email) {
                    $error .= "sorry email already taken !";
                }
                else {
                    if ($user->register($image, $firstName, $lastName, $email, $password)) {
                        $user->redirect("register.php?success");
                    }
                }
            }
            catch (PDOException $e) {
                echo $e->getMessage();
            }
        } 
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Userinfo</title>
</head>
<body>
    <form method="POST" enctype="multipart/form-data">
        <?= $error; ?>
        <?php
            if (isset($_GET["success"])) {
                echo "Successfully Registered!";
                $firstName = "";
                $lastName = "";
                $email = "";
            }
        ?>
        <h2>User Information</h2>
        <label for="firstName">First Name</label>
        <input type="text" name="firstName" value="<?= $firstName; ?>">
        <br>
        <label for="laslastNamet_name">Last Name</label>
        <input type="text" name="lastName" value="<?= $lastName; ?>">
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" value="<?= $email; ?>">
        <br>
        <label for="password">Password</label>
        <input type="password" name="password">
        <br>
        <label for="confirm_password">Confirm Password</label>
        <input type="password" name="confirm_password">
        <br>
        <label for="firstName">Image</label>
        <input type="file" name="image">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
    <br>
    <p> Already have an account? <a href="Login.php">Login here</a></p>
</body>
</html>