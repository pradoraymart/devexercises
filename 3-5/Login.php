<?php
    require_once 'db_connection.php';

    $error = "";
    if ($user->is_loggedin() != "") {
        $user->redirect('Welcome.php');
    }

    if (isset($_POST['submit'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
      
        if ($user->login($email, $password)) {
            $user->redirect('Welcome.php');
        } else {
            $error .= "Incorrect Email or Password!";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login Form</title>
</head>
<body>
    <form method="POST">
        <h2> Login Form </h2>
        <?= $error;?>
        <input type="email" name="email" required><br>
        <input type="password" name="password" required><br>
        <input type="submit" name="submit">
    </form>
    <br>
    <p> Don't have an account? <a href="Register.php">Register here</a></p>
</body>
</html>