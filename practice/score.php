<?php
	require "db_connection.php";
	session_start();

	$score = $_SESSION['score'];
	$total = $_SESSION['total'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Random Quiz</title>
</head>
<body>
	<h1>Congratulations!</h1>
	<h4>You've successfully finish the quiz</h4>

	<p>Youre total score is:<strong><?= $score . "/" . $total ;?></strong></p>
	<a href="quiz.php">Take Again</a>
	<?php 
		session_destroy();
	?>
</body>
</html>