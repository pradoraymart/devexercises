<?php
	$db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "yns";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    // Display 3-1
    $query3_1 = $db_conn->prepare('SELECT * from employees WHERE last_name LIKE "K%"');
    $query3_1->execute();

    // Display 3-2
    $query3_2 = $db_conn->prepare('SELECT * from employees WHERE last_name LIKE "%i"');
    $query3_2->execute();

    // Display 3-3
    $query3_3 = $db_conn->prepare('SELECT id, concat(first_name," ", last_name) AS "Employee Name" FROM employees WHERE hire_date BETWEEN "2015-01-01" AND "2015-03-31"');
    $query3_3->execute();

    // Display 3-4
    $query3_4 = $db_conn->prepare('SELECT b.last_name AS "Employee Last Name", e.last_name AS "Boss Last Name" FROM employees e INNER JOIN employees b ON e.id = b.boss_id WHERE b.last_name IS NOT NULL');
    $query3_4->execute();

    // Display 3-5
    $query3_5 = $db_conn->prepare('SELECT e.last_name AS "Employee Last Name" FROM employees e INNER JOIN departments d ON e.department_id = d.id WHERE d.name = "Sales" ORDER BY last_name DESC');
    $query3_5->execute();

    // Display 3-6
    $query3_6 = $db_conn->prepare('SELECT count(*) AS "No. of Employee with Middle Name" FROM employees WHERE middle_name IS NOT NULL');
    $query3_6->execute();

    // Display 3-7
    $query3_7 = $db_conn->prepare('SELECT count(*) AS Count, d.name AS "Department Name" FROM employees e INNER JOIN departments d ON e.department_id = d.id GROUP BY d.name');
    $query3_7->execute();

    // Display 3-8
    $query3_8 = $db_conn->prepare('SELECT id, CONCAT_WS( " ", first_name,last_name) AS "Employee Name", hire_date AS "Hire Date" FROM employees where hire_date = (SELECT max(hire_date) FROM employees)');
    $query3_8->execute();

    // Display 3-9
    $query3_9 = $db_conn->prepare('SELECT name AS "Department Name" FROM departments d WHERE d.name not IN(SELECT d.name FROM employees e INNER JOIN departments d ON e.department_id = d.id)');
    $query3_9->execute();

    // Display 3-9
    $query3_10 = $db_conn->prepare('SELECT CONCAT_WS(" ", first_name, last_name) AS "Employee Name" FROM employees e, positions p INNER JOIN employee_positions ep ON p.id = ep.position_id HAVING count(ep.employee_id) > 2');
    $query3_10->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>SQL Problems</title>
</head>
<body>
    <table>
        <h1>SQL 3-1</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_1 = $query3_1->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_1["id"]; ?></td>
                        <td><?= $res3_1["first_name"]; ?></td>
                        <td><?= $res3_1["last_name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-2</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_2 = $query3_2->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_2["id"]; ?></td>
                        <td><?= $res3_2["first_name"]; ?></td>
                        <td><?= $res3_2["last_name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-3</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Employee Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_3 = $query3_3->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_3["id"]; ?></td>
                        <td><?= $res3_3["Employee Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-4</h1>
        <thead>
            <tr>
                <th>Employee Last Name</th>
                <th>Boss Last Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_4 = $query3_4->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_4["Employee Last Name"]; ?></td>
                        <td><?= $res3_4["Boss Last Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-5</h1>
        <thead>
            <tr>
                <th>Employee Last Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_5 = $query3_5->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_5["Employee Last Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-6</h1>
        <thead>
            <tr>
                <th>No. of Employee with Middle Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_6 = $query3_6->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_6["No. of Employee with Middle Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-7</h1>
        <thead>
            <tr>
            	<th>Count</th>
                <th>Department Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_7 = $query3_7->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                    	<td><?= $res3_7["Count"]; ?></td>
                        <td><?= $res3_7["Department Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-8</h1>
        <thead>
            <tr>
                <th> ID </th>
                <th>Employee Name</th>
                <th>Hire Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_8 = $query3_8->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_8["id"]; ?></td>
                        <td><?= $res3_8["Employee Name"]; ?></td>
                        <td><?= $res3_8["Hire Date"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-9</h1>
        <thead>
            <tr>
                <th> Department Name </th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_9 = $query3_9->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_9["Department Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <table>
        <h1>SQL 3-10</h1>
        <thead>
            <tr>
                <th> Employee Name </th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res3_10 = $query3_10->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res3_10["Employee Name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>
</html>