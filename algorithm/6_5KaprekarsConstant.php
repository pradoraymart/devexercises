<?php

if (isset($_POST["submit"])) {
    $number = $_POST["number"];
    echo "The number of iteration to meet Kaprekars Constant " . KaprekarsConstant($number);
}

function KaprekarsConstant($number) {
    $counter = 0;
    while ($number != 6174) {
        $number = sortDesc($number) - sortAsc($number);
        $counter++;
    }
    return $counter;
}

function sortAsc($number) {
    $number = str_split($number);
    sort($number);
    return implode($number);
}

function sortDesc($number) {
    $number = str_split($number);
    rsort($number);
    while(count($number) < 4) {
        $number[] = 0;
    }
    return implode($number);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Kaprekars Constant</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST">
        <h2>Kaprekars Constant</h2>
        <label for="number"> Enter Number </label>
        <input type="number" name="number">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>