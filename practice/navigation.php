<!DOCTYPE html>
<html>
<head>
    <title>List of all exercises</title>
</head>
<body>
    <div>
        <h1> HTML and PHP Exercises </h1>
        <?php
            $phpFiles = [
                "Hello World",
                "Arithmetic Operations",
                "Greatest Common Divisor",
                "FizzBuzz Problem",
                "Dates",
                "Show user input",
                "Input Validation",
                "CSV File as Storage",
                "Display CSV File",
                "Upload Images",
                "Show Images",
                "Pagination",
                "Login Form"
                ];
            for ($i = 1; $i <= count($phpFiles); $i++){
                echo "<a href='../html_php/1-" .$i. ".php'>1-" .$i. " " .$phpFiles[$i - 1]. "</a><br>";
            }
        ?>
    </div>
    <div>
        <h1> Javascript Exercises </h1>
        <?php
            $jsFiles = [
                "Show Alert",
                "Confirm dialog",
                "Arithmetic Operations",
                "Prime Numbers",
                "Show Input",
                "Adding a Label",
                "Show Image link",
                "Background Color",
                "Scroll Up and Down",
                "Background Color Animation",
                "Image on Mouse Over",
                "Change Image size",
                "Images",
                "Date and Time"
                ];
            for ($j = 1; $j <= count($jsFiles); ++$j) {
                echo "<a href='../javascript/2-" .$j. ".html'>2-" .$j. " " .$jsFiles[$j - 1]. "</a><br>";
            }
        ?>
        <h1>Mysql Exercises </h1>
        <a href="../3-5/Login.php">Login and Registration</a>
    </div>
</body>
</html>