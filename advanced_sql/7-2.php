<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "therapists";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    // Display 7-2
    $query7_2 = $db_conn->prepare("SELECT * FROM `daily_work_shifts` INNER JOIN therapists ON `therapists`.`id` = `daily_work_shifts`.`therapists_id` ORDER BY (CASE 
            WHEN start_time <= '05:59:59' AND start_time >='00:00:00'
            THEN start_time
            ELSE target_date + 1
            END)
        ");
    $query7_2->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>7-2</title>
</head>
<body>
    <table>
        <h1>Advanced SQL 7-2</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Therapists Id</th>
                <th>Target Date</th>
                <th>Start time</th>
                <th>End Time</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res7_2 = $query7_2->fetch(PDO::FETCH_ASSOC)) {
            ?>
                    <tr>
                        <td><?= $res7_2["id"]; ?></td>
                        <td><?= $res7_2["therapists_id"]; ?></td>
                        <td><?= $res7_2["target_date"]; ?></td>
                        <td><?= $res7_2["start_time"]; ?></td>
                        <td><?= $res7_2["end_time"]; ?></td>
                        <td><?= $res7_2["name"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>
</html>