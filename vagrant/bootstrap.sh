
##@@APACHE
sudo yum install -y httpd


if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

if [ ! -d /var/www/html/ ]; then
    mkdir -p  /var/www/html
fi

##@@PHP
sudo yum -y install php
sudo yum -y install php-mysql
sudo yum -y install php-gd php-ldap php-odbc php-pear php-xml php-xmlrpc php-mbstring php-snmp php-soap curl curl-devel php-mcrypt
sudo service httpd start

##@@TOOLS
yum -y install curl git
