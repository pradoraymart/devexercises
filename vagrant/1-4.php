<!DOCTYPE html>
<html>
<head>
    <title>FizzBuzz Problem</title>
</head>
<body>
    <form method="POST">
        <h2>FizzBuzz</h2>
        <label for="number"> Insert Number</lable>
        <input type="text" name="number">
        <input type="submit" name="submit"><br>
    </form>
</body>

<?php
    if (isset($_POST["number"])) {
        if (!empty($_POST["number"])) {
            $number = $_POST["number"];
            if (is_numeric($number)) {
                for ($count = 1 ; $count < $number + 1 ; $count++) {
                    if ($count % 15 == 0) {
                        echo "FizzBuzz <br>";
                    } elseif ($count % 3 == 0 ) {
                        echo "Fizz <br>";
                    } elseif ($count % 5 == 0) {
                        echo "Buzz <br>";
                    } else {
                        echo $count . '<br>';
                    }
                }
            } else {
                echo "Please insert a number only";
            }
        } else {
            echo "Please insert a number";
        }
    }
?>
</html>