<?php
//Define server and database credentials
define("DB_SERVER","localhost");
define("DB_USERNAME","root");
define("DB_PASSWORD","");
define("DB_NAME","quiz");

//Connect to MySQL Database
$conn = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_NAME);

//Check the connection
if ($conn == false) {
	die("Error: Cannot connect to the server" . mysqli_connect_error());
} else {
	//echo "Successfully connected!";
}
?>
