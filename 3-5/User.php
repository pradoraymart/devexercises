<?php

class User {

    private $db;

    function __construct($db_conn) {
        $this->db = $db_conn;
    }

    public function register($image, $firstName, $lastName, $email, $password) {

        try {
            $folder = "images/";
            $image = $_FILES["image"]["name"];
            $path = $folder . $image ;
            $targetFile = $folder . basename($_FILES["image"]["name"]);
            $imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
            move_uploaded_file( $_FILES["image"] ["tmp_name"], $path);

            $newPassword = password_hash($password, PASSWORD_DEFAULT);

            $query = $this->db->prepare("INSERT INTO users(image, first_name, last_name, email, password)VALUES(:image, :firstName, :lastName, :email, :password)");
              
            $query->bindparam(":image", $image);
            $query->bindparam(":firstName", $firstName);
            $query->bindparam(":lastName", $lastName);
            $query->bindparam(":email", $email);
            $query->bindparam(":password", $newPassword);
            $query->execute();

            return $query;
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function login($email, $password) {
        try {
            $query = $this->db->prepare("SELECT * FROM users WHERE email = :email LIMIT 1");
            $query->execute(array(":email" => $email));
            $userRow = $query->fetch(PDO::FETCH_ASSOC);
            if ($query->rowCount() > 0) {
                if (password_verify($password, $userRow["password"])) {
                    $_SESSION["userID"] = $userRow["id"];
                    return true;
                } else {
                return false;
                }
            }
        } catch (PDOException $e) {
           echo $e->getMessage();
        }
    }

   public function is_loggedin() {
        if (isset($_SESSION["userID"])) {
        return true;
        }
    }

    public function redirect($url) {
        header("Location: $url");
    }

   public function logout() {
        session_destroy();
        unset($_SESSION["userID"]);
        return true;
    }
}
?>