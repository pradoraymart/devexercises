<?php

function PentagonalNumber($number) {

    $result = 0;
    for ($i = 1; $i < $number; $i++) {
        $result = (($i * 5) + ($result - $i)) + $i;
    }
    return $result + 1;
}
if (isset($_POST["submit"])) {
    $number = $_POST["number"];
    echo "The number of dots in a pentagon for the " . $number ."th Iteration is " . PentagonalNumber($number);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Pentagonal Number</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST">
        <h2>Pentagonal Numbers</h2>
        <label for="number"> Enter Number </label>
        <input type="number" name="number">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>