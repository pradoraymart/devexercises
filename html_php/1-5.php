<!DOCTYPE html>
<html>
<head>
    <title>Input date</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST">
        <h2>Show Input Date</h2>
        <label for="date"> Date </label>
        <br>
        <input type="date" name="date">
        <button type="submit" name="submit"> Submit </button>
    </form>
</body>

<?php 
    if (isset($_POST["submit"]) && !empty($_POST["date"])) {
        $date = $_POST["date"];
        echo "The third day after " . $date . " is " . getThirdDate($date);
    } elseif (isset($_POST["submit"]) && empty($_POST["date"])) {
        echo"Please insert a date";
    }

    function getThirdDate($date) {
        $thirdDate = date("Y-m-d D", strtotime($date . " + 3 days"));
        return $thirdDate;
    }
?>
</html>