<?php
    $openFile = fopen("1-10.csv", "r") or die ("Cannot open the file");
    $fp = file("1-10.csv");
    $numOfRows = count($fp);
    $start = 1;
    $end = 10;
    $url = $_SERVER["REQUEST_URI"];
    if (isset($_GET["page"])) {
        // Get page number
        $currentPage = $_GET["page"];
        $perPage = 10;
        // Get the start of the data to display
        $start = (($currentPage * $perPage) - $perPage) + 1;
        // Get the end of the data to display
        $end = $start + ($perPage - 1);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Show images</title>
</head>
<body>
    <table>
        <h2>User Information</h2>
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Age</th>
                <th>Email</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while (($data = fgetcsv($openFile, 1000, ",")) !== false) {
                    $num = count($data);
                    $totalPages = ceil ($numOfRows / 10);
                    $csv[] =  $data;
                    // Check the presence of the data
                    if ($num > 0) {
                        if ($start <= $end) { 
                            echo "<tr>";
                            echo "<td> <img width='50px' height='auto' src='../images/$data[0]'></td>";
                            for ($i = 1; $i < $num; $i++) {
                                echo "<td>" . $data[$i] . "</td>";
                            }
                            $start++;
                        }
                    } else {
                        echo "No Data Available";
                    }
                    echo "</tr>";
                }
                fclose($openFile);
            ?>
        </tbody>
    </table>
    <?php
        $pageLink = "";
        for ($count = 1; $count <= $totalPages; $count++) {
            $pageLink .= "<a href='1-12.php?page=" .$count. "'>" .$count. "</a>";
        } 
        echo $pageLink;
    ?>
</body>
</html>