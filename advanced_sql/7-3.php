<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "yns";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    // Display 7-1
    $query7_3 = $db_conn->prepare("SELECT `emp`.*, 
        (CASE `pos`.`name`
            WHEN 'CEO' THEN 'Chief Executive Officer'
            WHEN 'CTO' THEN 'Chief Technical Officer'
            WHEN 'CFO' THEN 'Chief Financial Officer'
            ELSE `pos`.`name` 
            END) AS Position from employees emp
        INNER JOIN employee_positions emppos ON `emppos`.`employee_id` = `emp`.`id`
        INNER JOIN positions pos ON `emppos`.`position_id` = `pos`.`id`");
    $query7_3->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>7-3</title>
</head>
<body>
    <table>
        <h1>Advanced SQL 7-3</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Birth Date</th>
                <th>Hire Date</th>
                <th>Position</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res7_3 = $query7_3->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res7_3["id"]; ?></td>
                        <td><?= $res7_3["first_name"]; ?></td>
                        <td><?= $res7_3["last_name"]; ?></td>
                        <td><?= $res7_3["middle_name"]; ?></td>
                        <td><?= $res7_3["birth_date"]; ?></td>
                        <td><?= $res7_3["hire_date"]; ?></td>
                        <td><?= $res7_3["Position"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>
</html>