<?php

if (isset($_POST["caseOne"])) {
    $input = "???rrurdr?";
    echo CorrectPath($input);
}
if (isset($_POST["caseTwo"])) {
    $input = "drdr??rrddd?";
    echo CorrectPath($input);
}

function CorrectPath($str) {
  
    $newString = str_split($str);
    $max = strlen($str);
    for ($i = 0; $i <= $max; $i++) {
        if ($newString[$i] == "?") {
            if (substr_count($str, "r") >= 4 && substr_count($str, "d") >= 4) {
                $newString[$i] = "u";
            } else {
                $newString[$i] = "d";
            }
            $newString[$max - 1] = "d";
        }
    }
    return implode($newString);
}
echo '<br><br>Input:"???rrurdr?"<br>';
echo 'Output:"dddrrurdrd"<br>';
echo 'Input:"drdr??rrddd?"<br>';
echo 'Output:"drdruurrdddd"<br>';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Correct Path</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST">
        <h2>Correct Path</h2><br>
        <button type="submit" value="submit" name="caseOne">Run Test Case 1</button>
        <button type="submit" value="submit" name="caseTwo">Run Test Case 2</button>
    </form>
</body>
</html>