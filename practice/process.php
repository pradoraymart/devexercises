<?php
    require "db_connection.php";
    $score = 0;
    // Check if the submit has been click
    if (isset($_POST['submit'])) {
        if (isset($_POST['choice'])) {
            for ($i = 1 ; $i <= 10 ; $i++) {
                $answerValue = '"' .$_POST['choice'][$i].'"'.",";
                $answerValue = $answerValue .'"a"';
                // Count the correct value that has 1 in orrect column and present from the user answer
                $sqlAnswer = "SELECT count(*) FROM answers WHERE answer IN ($answerValue) AND correct = '1'"; 
                $result = mysqli_query($conn, $sqlAnswer);
                $data = mysqli_fetch_array($result);
                $count =  $data['count(*)'];
                if ($count == 1) {
                    $score++;
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Score</title>
</head>
<body>
    <h1> Congratulations! </h1>
    <h3>You have already finished the test!</h3>
    <p>Your final score is:<strong> <?= $score;?></strong></p>
    <br>
    <a href="quiz.php">Take Test Again</a>
</body>
</html>