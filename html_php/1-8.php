<?php
    $error = "";
    $name = "";
    $age = "";
    $email = "";
    $address = "";

    function cleanInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST["name"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $address = $_POST["address"];

        if (!empty($name)) {
            if(preg_match("/^[a-zA-Z\.]+$/", $name)) {
                $name = cleanInput($name);
            } else {
                $error .= "Please insert text only for name";
            }
        } else {
            $error .= "Please insert a name";
        }
        if (!empty($age)) {
            if(is_numeric($age)) {
                $age = cleanInput($age);
            } else {
                $error .= "Please insert a number for age";
            }
        } else {
            $error .= "Please insert an age";
        }
        if (!empty($email)) {
            $regex = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/";
            if (preg_match($regex, $email)) {
                $email = cleanInput($email);
            } else {
                echo "Invalid Email Address";
            }
        } else {
            $error .= "Please insert an email";
        }
        if (!empty($address)) {
            $address = cleanInput($address);
        } else {
            $error .= "<p> Please insert an address";
        }
        if ($error == "") {
            $fileOpen = fopen("1-8.csv","a");
            $formData = array(
                "name"  => $name,
                "age"  => $age,
                "email" => $email,
                "address" => $address
            );
            fputcsv($fileOpen, $formData);
            echo "</i>Successfully save data!</i>";
            $name = "";
            $age = "";
            $email = "";
            $address = "";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Userinfo</title>
</head>
<body>
    <form action = "<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <?= $error; ?>
        <h2>User Information</h2>
        <label for="name"> Name </label>
        <input type="text" name="name" value="<?= $name; ?>">
        <br>
        <label for="age"> Age </label>
        <input type="text" name="age" value="<?= $age; ?>">
        <br>
        <label for="email"> Email </label>
        <input type="text" name="email" value="<?= $email; ?>">
        <br>
        <label for="address"> Address </label>
        <input type="text" name="address" value="<?= $address; ?>">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>