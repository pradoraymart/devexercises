<?php
    $data[] = array("(2,1)", "(6,3)", "(4,3)", "(8,4)", "(3,4)", "(1,6)", "(7,7)", "(5,8)");

    // Convert array
    for($i = 0; $i < 8; $i++) {
        $x = 0;
        $queen = $data[$x][$i];
        $array[][] = $queen;
    }

    $diagonalUp = getDiagonalUp($array);
    $diagonalDown = getDiagonalDown($array);
    $horizontal = getHorizontal($array);
    $vertical = getVertical($array);

    if (!empty($diagonalUp)) {
        echo $diagonalUp;
    } else if (!empty($vertical)) {
        echo $vertical;
    } else if (!empty($diagonalDown)) {
        echo $diagonalDown;
    } else if (!empty($horizontal)) {
        echo $horizontal;
    }else {
        echo "true";
    }

    // Check Diagonal UP
    function getDiagonalUp(array $array){
        $iteration = 0;
        do {
            for ($i = 0; $i < count($array); $i++ ) {
                for($j = 0; $j < $i; $j++) {
                    if($iteration == 8){
                        $iteration -= 1;
                    }
                    if ($array[$i] == $array[$iteration]) {
                        $i = $i + 1;
                    }
                    if($i == 8) {
                        $i = $i - 1;
                    }
                    $diagonal = checkDiagonalUp($array[$iteration]);
                    $currentDiagonal = checkDiagonalUp($array[$i]);
                    if ($diagonal == $currentDiagonal && $array[$iteration] != $array[$i]) {
                        return implode($array[$iteration]);
                        break;
                        $iteration = 8;
                    }
                }
            }
            $iteration += 1;
        } while ($iteration < 8);
    }

    // Check Horizontal
    function getHorizontal(array $array) {
       for ($i = 0; $i < count($array); $i++ ) {
            $next = 0;
            if ($array[$i] == $array[$next]){
                $i = $i + 1;
            }
            $vertical = checkHorizontal($array[$next]);
            $currentData = checkHorizontal($array[$i]);
            if ($vertical == $currentData) {
                return ($array[$next]);
            }
            $next += 1;
        }
    }

   //Check Horizontal
    function getVertical(array $array) {
        for ($i = 0; $i < count($array); $i++ ) {
            $next = 0;
            if ($array[$i] == $array[$next]){
                $i = $i + 1;
            }
            $vertical = checkVertical($array[$next]);
            $currentData = checkVertical($array[$i]);
            if ($vertical == $currentData) {
                return ($array[$next]);
            }
            $next += 1;
        }
    }

    //Check Diagonal Down
     function getDiagonalDown(array $array){
        $iteration = 0;
        do {
            for ($i = 0; $i < count($array); $i++ ) {
                for ($j = 0; $j < $i; $j++) {
                    if ($iteration == 8){
                        $iteration -= 1;
                    }
                    if ($array[$i] == $array[$iteration]) {
                        $i = $i + 1;
                    }
                    if ($i == 8) {
                        $i = $i - 1;
                    }
                    $diagonal = checkDiagonalDown($array[$iteration]);
                    $currentDiagonal = checkDiagonalDown($array[$i]);
                    if ($diagonal == $currentDiagonal && $array[$iteration] != $array[$i]) {
                        return implode($array[$iteration]);
                        break;
                        $iteration = 8;
                    }
                }
            }
            $iteration += 1;
        } while ($iteration < 8);
    }

    function checkHorizontal(array $number) {
        $result = 0;
        $strData = implode($number);
        $result = substr($strData, 1,1);
        return $result;
    }

    function checkVertical(array $number) {
        $result = 0;
        $strData = implode($number);
        $result = substr($strData, 3,3);
        return $result;
    }

    function checkDiagonalDown(array $number) {
        $result = 0;
        $strData = implode($number);
        $getX = substr($strData, 1,1);
        $getY = substr($strData, 3,3);
        $result = intval($getX) + intval($getY);
        return $result;
    }
    
    function checkDiagonalUp(array $number) {
        $result = 0;
        $strData = implode($number);
        $getX = substr($strData, 1,1);
        $getY = substr($strData, 3,3);
        $result = intval($getX) - intval($getY);
        return $result;
    }
?>