<?php
    
function MaximalSquare($strArr) {

  $rows = strlen($strArr[0]);
  $col = count($strArr);
  $matrix[][] = array($rows, $col );
  $maxsquare = 0;
  for ($i = 0; $i < $rows; $i++) {
        for($j = 0; $j < $col; $j++) {
            if($i == 0 || $j == 0) {
                $matrix[$i][$j] = 1;
            } else {
                // Check if adjacent
                // elements are equal
                if ($strArr[$i][$j] == $strArr[$i - 1][$j] &&
                    $strArr[$i][$j] == $strArr[$i][$j - 1] &&
                    $strArr[$i][$j] == $strArr[$i - 1][$j - 1]
                ) { 
                    $matrix[$i][$j] = min(min($matrix[$i - 1][$j], $matrix[$i][$j - 1]), $matrix[$i - 1][$j - 1] ) + 1;
                } else {
                    $matrix[$i][$j] = 1;
                }
            }
            $maxsquare = max($maxsquare, $matrix[$i][$j]);
        }
    }
    return $maxsquare * $maxsquare;
}
$data = array("0111", "1111", "1111", "1111");
echo "The Maximal Square is: " . MaximalSquare($data);
?>