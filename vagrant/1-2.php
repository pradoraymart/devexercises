<?php
	$error = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!empty($_POST["firstNumber"]) && !empty($_POST["secondNumber"])) {
            $firstNumber = $_POST["firstNumber"];
            $secondNumber = $_POST["secondNumber"];
            echo "<br>";
            echo "Sum of the two Numbers: " . sumOfTwoNumbers($firstNumber, $secondNumber) . "<br>";
            echo "Difference of the two Numbers: " . differenceOfTwoNumbers($firstNumber, $secondNumber) . "<br>";
            echo "Product of the two Numbers: " . productOfTwoNumbers($firstNumber, $secondNumber) . "<br>";
            echo "Quoitent of the two Numbers: " . quotientOfTwoNumbers($firstNumber, $secondNumber) . "<br>";
        } elseif (empty($_POST["firstNumber"])) {
            $error .= "Please insert the first number <br>";
        } elseif (empty($_POST["secondNumber"])) {
            $error .= "Please insert the second number <br>";
        }
    }
    // Addition
    function sumOfTwoNumbers($firstNumber, $secondNumber) {
        $total = $firstNumber + $secondNumber;
        return $total;
    }
    // Subtraction
    function differenceOfTwoNumbers($firstNumber, $secondNumber) {
        $total = $firstNumber - $secondNumber;
        return $total;
    }
    // Multiplication
    function productOfTwoNumbers($firstNumber, $secondNumber) {
        $total = $firstNumber * $secondNumber;
        return $total;
    }
    // Division
    function quotientOfTwoNumbers($firstNumber, $secondNumber) {
        $total = $firstNumber / $secondNumber;
        return $total;
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Four Arithmetic Operations</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
		<h2>Four Arithmetic Calculator</h2>
        <?= $error; ?><br>
		<label for="firstNumber"> First Number </label>
		<input type="number" name="firstNumber">
		<br>
		<label for="secondNumber"> Second Number </label>
		<input type="number" name="secondNumber">
		<br>
		<input type="submit" value="submit" name="submit">
	</form>
</body>
</html>