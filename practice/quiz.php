<?php
    require "db_connection.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title>Random Quiz</title>
</head>
<body>
    <h1> ICT QUIZ </h1>
    <form method="post" action="process.php">
    <?php
        // Randomly get all the questions
        $sqlQuestions = "SELECT * FROM questions ORDER BY rand() ";
        $result = mysqli_query($conn, $sqlQuestions);

        // For questions number
        $i = 1;
        while ($row = mysqli_fetch_array($result)) {
            echo"<p>" .$i. " " .$row["questions"]. "<br>";
            $number = $row["idquestions"];

            // Get all the answer depending on questions id
            $sqlAnswers = "SELECT * FROM answers WHERE id_questions = "$number"";
            $resultAnswer = mysqli_query($conn, $sqlAnswers);

            // Display the choices in radio button
            while ($rowAns = mysqli_fetch_array($resultAnswer)) {
                echo "<input type='radio' value=''" .$rowAns["answer"]. "name='choice[" .$i. "]'>" .$rowAns["answer"]. "</input><br>";
            }
            $i++;
        }
        echo"<hr>";
        echo"<input type='submit' name='submit'>";
    ?>
    </form>
</body>
</html>