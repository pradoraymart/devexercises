<?php
    include_once "db_connection.php";

    if (!$user->is_loggedin()) {
        $user->redirect("Login.php");
    }
    $userID = $_SESSION["userID"];
    $query = $db_conn->prepare("SELECT * FROM users WHERE id = :userID");
    $query->execute(array(":userID" => $userID));
    $userRow = $query->fetch(PDO::FETCH_ASSOC);

    $endLimit = 10;
    $query = "SELECT * FROM users";

    $sql = $db_conn->prepare($query);
    $sql->execute();
    $total_results = $sql->rowCount();
    $total_pages = ceil($total_results / $endLimit);

    if (!isset($_GET["page"])) {
        $page = 1;
    } else {
        $page = $_GET["page"];
    }
    $startingLimit = ($page - 1) * $endLimit;
    $end = $startingLimit + ($endLimit - 1);
    $dataSql = $db_conn->prepare("SELECT id, image, first_name, last_name, email FROM users LIMIT $startingLimit, $endLimit");
    $dataSql->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome Page</title>
    <style>
        table{
            width: 60%;
            margin: 0px 50px 0px 20%;
        }
        img{
            width:100px;
            height:auto;
        }
        th {
            vertical-align:middle;
            text-align: center;
            padding:10px;
        }
        td {
            vertical-align:top;
            text-align: left;
            padding:10px;
        }
        table, th, td {
            border: 2px solid gray;
            border-collapse: collapse;
        }
        h1, h2, .profile {
            text-align: center;
        }
        .pagination {
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <h2>Welcome <?= $userRow["first_name"] ." ". $userRow["last_name"]?>!</h2>
    <div class="profile">
        <img src="../images/<?= $userRow["image"];?>">
        <form method="POST">
            <button type="submit" name="submit">Logout</button>
        </form>
        <?php
            if (isset($_POST["submit"])) {
                $user->logout();
                $user->redirect("Login.php");
            }
        ?>
    </div>
    <table>
        <h1>List of all users</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($row = $dataSql->fetch(PDO::FETCH_ASSOC)) {
            ?>
                <tr>
                    <td><?= $row["id"]; ?></td>
                    <td><img src="../images/<?= $row["image"];?>"></td>
                    <td><?= $row["first_name"]; ?></td>
                    <td><?= $row["last_name"]; ?></td>
                    <td><?= $row["email"]; ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
    <div class="pagination">
        <?php
        for ($page = 1; $page <= $total_pages; $page++) { ?>
            <a href="<?="?page=$page"; ?>" class="links"><?= $page; } ?>
            </a>
    </div>
</body>
</html>