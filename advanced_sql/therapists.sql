-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2019 at 03:09 AM
-- Server version: 5.5.62
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `therapists`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_shifts`
--

CREATE TABLE `daily_work_shifts` (
  `id` int(11) NOT NULL,
  `therapists_id` int(11) NOT NULL,
  `target_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_work_shifts`
--

INSERT INTO `daily_work_shifts` (`id`, `therapists_id`, `target_date`, `start_time`, `end_time`) VALUES
(1, 1, '2019-06-17 07:10:28', '14:00:00', '15:00:00'),
(2, 2, '2019-06-17 07:10:28', '22:00:00', '23:00:00'),
(3, 3, '2019-06-17 07:12:01', '00:00:00', '01:00:00'),
(4, 4, '2019-06-17 07:12:01', '05:00:00', '05:30:00'),
(5, 1, '2019-06-17 07:13:56', '21:00:00', '21:45:00'),
(6, 5, '2019-06-17 07:13:56', '05:30:00', '05:20:00'),
(7, 3, '2019-06-17 07:14:30', '02:00:00', '02:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `therapists`
--

CREATE TABLE `therapists` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `therapists`
--

INSERT INTO `therapists` (`id`, `name`) VALUES
(1, 'John'),
(2, 'Arnold'),
(3, 'Robert'),
(4, 'Ervin'),
(5, 'Smith');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `therapists_id` (`therapists_id`);

--
-- Indexes for table `therapists`
--
ALTER TABLE `therapists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `therapists`
--
ALTER TABLE `therapists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD CONSTRAINT `daily_work_shifts_ibfk_1` FOREIGN KEY (`therapists_id`) REFERENCES `therapists` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
