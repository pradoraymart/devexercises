<?php
    session_start();
    if (isset($_SESSION["email"])) {
        $email = $_SESSION["email"];
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome Page</title>
</head>
<body>
    <h2>Welcome <?= $email; ?></h2>
    <a href="logout.php"><button type="submit" name="submit">Logout</button></a>
</body>
</html>