<?php 

function FirstFactorial($number) {

    $factorial = 1;
    for($i = 1; $i <= $number; $i++) {
        $factorial *= $i;
    }
    return $factorial; 
}

if (isset($_POST["submit"])) {
    $number = $_POST["number"];
    echo "The factorial of the number" . $number ." is " . FirstFactorial($number);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Factorial Numbers</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST">
        <h2>Factorial Numbers</h2>
        <label for="number"> Enter Number </label>
        <input type="number" name="number">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>