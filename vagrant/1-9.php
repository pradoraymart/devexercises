<!DOCTYPE html>
<html>
<head>
    <title>CSV Table</title>
</head>
<body>
    <table>
        <caption> CSV Table </caption>
        <thead>
            <tr>
                <th> Name </th>
                <th> Age </th>
                <th> Email </th>
                <th> Address </th>
            </tr>
        </thead>
        <tbody>
            <?php
                $openFile = fopen("1-8.csv", "r") or die ("Cannot open the file");
                while (($data = fgetcsv($openFile, 0, ",")) !== false) {
                    $i = 0;
                    echo '<tr>';
                    foreach ($data as $row) {
                        echo '<td>' . $row . '</td>';
                        $i++;
                    }
                    echo '</tr>';
                }
                fclose($openFile);
            ?>
        </tbody>
    </table>
</body>
</html>