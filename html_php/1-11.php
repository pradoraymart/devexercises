<!DOCTYPE html>
<html>
<head>
    <title>Show images</title>
</head>
<body>
    <table>
        <h2>User Information</h2>
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Age</th>
                <th>Email</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $openFile = fopen("1-10.csv", "r") or die ("Cannot open the file");
                while (($data = fgetcsv($openFile, 0, ",")) !== false) {
                    $num = count($data);
                    if ($num > 0){
                        echo "<tr>";
                        echo "<td> <img width='50px' height='auto' src='../images/$data[0]'></td>";
                        for ($i = 1; $i < $num; $i++) {
                            echo "<td>" . $data[$i] . "</td>";  
                        }
                        echo "</tr>";
                    } else {
                        echo "No Data Available";
                    }
                }
                fclose($openFile);
            ?>
        </tbody>
    </table>
</body>
</html>