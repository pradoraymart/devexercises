<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "yns";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    // Display 7-1
    $query7_4 = $db_conn->prepare("SELECT * FROM `7_4`");
    $query7_4->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>7-4</title>
</head>
<body>
    <table>
        <h1>Advanced SQL 7-4</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Parent Id</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res7_4 = $query7_4->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <tr>
                        <td><?= $res7_4["id"]; ?></td>
                        <td><?= $res7_4["parent_id"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>
</html>