<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "yns";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    // Display 7-1
    $query7_1 = $db_conn->prepare("SELECT *, FLOOR(DATEDIFF(NOW(), birth_date )/365) as Age  FROM employees WHERE FLOOR(DATEDIFF(NOW(), birth_date ) / 365) BETWEEN 40 AND 50");
    $query7_1->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>7-1</title>
</head>
<body>
    <table>
        <h1>Advanced SQL 7-1</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Birth Date</th>
                <th>Hire Date</th>
                <th>Age</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($res7_1 = $query7_1->fetch(PDO::FETCH_ASSOC)) {
            ?>
                    <tr>
                        <td><?= $res7_1["id"]; ?></td>
                        <td><?= $res7_1["first_name"]; ?></td>
                        <td><?= $res7_1["last_name"]; ?></td>
                        <td><?= $res7_1["middle_name"]; ?></td>
                        <td><?= $res7_1["birth_date"]; ?></td>
                        <td><?= $res7_1["hire_date"]; ?></td>
                        <td><?= $res7_1["Age"]; ?></td>
                    </tr>
            <?php
                }
            ?>
        </tbody>
    </table>
</body>
</html>