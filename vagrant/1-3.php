<?php
    if (isset($_POST["submit"])) {

        $firstNumber = $_POST["firstNumber"];
        $secondNumber = $_POST["secondNumber"];

        if (!empty($_POST["firstNumber"]) && !empty($_POST["secondNumber"])) {
            echo "The GCD of ". $firstNumber . " and " . $secondNumber . " is " . getGCF($firstNumber, $secondNumber);
        } elseif (empty($_POST["firstNumber"]) && empty($_POST["secondNumber"])) {
            echo "Please insert the first number <br>";
            echo "Please insert the second number"; 
        } elseif (!empty($_POST["firstNumber"]) && empty($_POST["secondNumber"])) {
            echo "Please insert the second number"; 
        } else {
            echo "Please insert the first number";
        }
    } 

    function getGCF($x, $y) {
        if ($x == 0 && $y == 0) {
            return 0;
        }
        if ($x == 0) {
            return $y;
        }
        if ($x > $y) {
            $x = $x - $y;
            return $y - $x;
        }
            return $y - $x;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Greatest Common Factor</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <form method="POST" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h2>Greatest Common Divisor Calculator</h2>
        <label for="firstNumber"> First Number </label>
        <input type="number" name="firstNumber">
        <br>
        <label for="secondNumber"> Second Number </label>
        <input type="number" name="secondNumber">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>