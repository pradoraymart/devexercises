<?php
    $error = "";
    $name = "";
    $age = "";
    $email = "";
    $address = "";

    function cleanInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST["name"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $address = $_POST["address"];

        // Validate name and accept text only
        if (!empty($name)) {
            if (preg_match("/^[a-zA-Z\.]+$/", $name)) {
                $name = cleanInput($name);
            } else {
                $error .= "Please insert text only for name";
            }
        } else {
            $error .= "Please insert a name";
        }
        // Validate age
        if (!empty($age)) {
            if (is_numeric($age)) {
                $age = cleanInput($age);
            } else {
                $error .= "Please insert a number for age";
            }
        } else {
            $error .= "Please insert an age";
        }
        // Validate email using regular expression
        if (!empty($email)) {
            $emailValidation = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/";
            if (preg_match($emailValidation, $email)) {
                $email = cleanInput($email);
            } else {
                echo "Invalid Email Address";
            }
        } else {
            $error .= "Please insert an email";
        }
        // Validate address
        if (!empty($address)) {
            $address = cleanInput($address);
        } else {
            $error .= "Please insert an address";
        }
        // Check is the file is set
        if (isset($_FILES["file"]["name"])) {
            $fileName = $_FILES["file"]["name"];
            $tmpName = $_FILES["file"]["tmp_name"];
            $pathName = "../images/" . basename($_FILES["file"]["name"]);
            if (!empty($fileName)) {
                $location = "../images/";
                $checkImage = getimagesize($_FILES["file"]["tmp_name"]);
                // Avoid Duplicate data
                if (!file_exists($pathName)) {
                    // Validate and accept images only
                    if ($checkImage !== false) {
                        // Save image name to csv file
                        if (move_uploaded_file($tmpName, $pathName)) {
                            // Save data to csv file
                            $fileOpen = fopen("1-10.csv", "a");
                            $formData = array(
                                "file" => $fileName,
                                "name"  => $name,
                                "age"  => $age,
                                "email" => $email,
                                "address" => $address
                                );
                            fputcsv($fileOpen, $formData);
                            $name = "";
                            $age = "";
                            $email = "";
                            $address = "";
                            $file = "";
                            echo "Successfully save data!";
                        } else {
                            echo "please choose a file";
                        }
                    } else {
                        $error .= "Please insert an image only";
                    }
                } else {
                    $error .= "Failed to save file. File already exists";
                }
            } else {
                $error .= "Please insert an image";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Userinfo</title>
</head>
<body>
    <form action = "<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data">
        <?= $error; ?>
        <h2>User Information</h2>
        <label for="name">Name</label>
        <input type="text" name="name" value="<?= $name; ?>">
        <br>
        <label for="age">age</label>
        <input type="text" name="age" value="<?= $age; ?>">
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" value="<?= $email; ?>">
        <br>
        <label for="address">Address</label>
        <input type="text" name="address" value="<?= $address; ?>">
        <br><br>
        <label for="file">Select image to upload : </label><br>
        <input type="file" name="file" id="file" accept="images/*">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>